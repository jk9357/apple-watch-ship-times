//
//  AppDelegate.m
//  Apple Watch Ship Times
//
//  Created by jk9357 on 20/04/2015.
//  Copyright (c) 2015 World Wide Meth Distribution Inc. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
