//
//  ViewController.m
//  Apple Watch Ship Times
//
//  Created by jk9357 on 20/04/2015.
//  Copyright (c) 2015 World Wide Meth Distribution Inc. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController {
    NSArray *watchNames;
    NSArray *watchShipTimes;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    CALayer *viewLayer = [CALayer layer];
    [viewLayer setBackgroundColor:CGColorCreateGenericRGB(0.0, 0.0, 0.0, 0.0)]; //RGB plus Alpha Channel
    [self.view setWantsLayer:YES]; // view's backing store is using a Core Animation Layer
    [self.view setLayer:viewLayer];    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    if (!watchNames||!watchShipTimes) {
        return 0;
    } else {
        return watchNames.count;
    }
}

- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {
    
    // Get an existing cell with the MyView identifier if it exists
    NSTextField *result = [tableView makeViewWithIdentifier:@"MyView" owner:self];
    
    // There is no existing cell to reuse so create a new one
    if (result == nil) {
        
        // Create the new NSTextField with a frame of the {0,0} with the width of the table.
        // Note that the height of the frame is not really relevant, because the row height will modify the height.
        result = [[NSTextField alloc] initWithFrame:CGRectMake(0, 0, tableColumn.width, 20)];
        
        // The identifier of the NSTextField instance is set to MyView.
        // This allows the cell to be reused.
        result.identifier = @"MyView";
    }
    result.backgroundColor=[NSColor clearColor];
    result.bezeled         = NO;
    result.editable        = NO;
    result.drawsBackground = NO;
    result.lineBreakMode=NSLineBreakByWordWrapping;
    // result is now guaranteed to be valid, either as a reused cell
    // or as a new cell, so set the stringValue of the cell to the
    // nameArray value at row
    if (!watchNames||!watchShipTimes) {
        return result;
    }
    if ([tableColumn.identifier isEqualToString:@"AutomaticTableColumnIdentifier.0"]) {
        result.stringValue = watchNames[row];
    } else {
        result.stringValue=watchShipTimes[row];
    }
    
    // Return the result
    return result;
}

-(void)viewDidAppear {
    _loadingSpinner.hidden=NO;
    _loadingLabel.hidden=NO;
    [_loadingSpinner startAnimation:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,
                                             (unsigned long)NULL), ^(void) {
        [self getShipTimesForWatch:_kindButton.indexOfSelectedItem forCountry:_countryButton.titleOfSelectedItem is42mm:NO];
    });
}

-(void)getShipTimesForWatch:(short)watchType forCountry:(NSString *)country is42mm:(BOOL)is42mm {
    NSDictionary *countries=@{@"Australia":@"au",@"Canada":@"ca",@"China":@"cn",@"France":@"fr",@"Germany":@"de",@"Hong Kong":@"hk",@"Japan":@"jp",@"United Kingdom":@"uk",@"United States":@"us"};
    NSString *url=[NSString stringWithFormat:@"http://store.apple.com/%@/buy-watch/%@",countries[country],watchType==0 ? @"apple-watch-sport" : (watchType==1 ? @"apple-watch" : @"apple-watch-edition")];
    
    NSError *error;
    NSString *sportWatchOverviewPage=[NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"An error occured while attempting to fetch data: %@",error);
    }
    NSArray *a=[sportWatchOverviewPage componentsSeparatedByString:@"class=\"product-list\""];
    sportWatchOverviewPage=a[1];
    a=[sportWatchOverviewPage componentsSeparatedByString:@"</ul>"];
    sportWatchOverviewPage=a[0];
    a=[sportWatchOverviewPage componentsSeparatedByString:@"\n"];
    NSMutableArray *b=[[NSMutableArray alloc]initWithArray:a];
    [b removeObjectAtIndex:0];
    BOOL foundWatch = '\0';
    BOOL foundName = '\0';
    NSMutableArray *watches=[[NSMutableArray alloc]init];
    NSMutableArray *shipTimes=[[NSMutableArray alloc]init];
    for (int i=0; i<b.count; i++) {
        if (!foundWatch) {
            if ([b[i]rangeOfString:@"<li class=\"product-item "].location!=NSNotFound) {
                foundWatch=YES;
                NSLog(@"Found watch");
                i+=2;
            }
        } else {
            if (!foundName) {
                if ([b[i]rangeOfString:@"<div class=\"product-item__title \">"].location!=NSNotFound) {
                    NSString *name=[b[i]stringByReplacingOccurrencesOfString:@"<div class=\"product-item__title \">" withString:@""];
                    name=[name stringByReplacingOccurrencesOfString:@"</div>" withString:@""];
                    name=[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    NSLog(@"Name: %@",name);
                    [watches addObject:name];
                    foundName=YES;
                }
            } else {
                if ([b[i]rangeOfString:@"<form method=\"get\" action=\"#\"  "].location!=NSNotFound) {
                    NSArray *c=[b[i]componentsSeparatedByString:@"Availability | "];
                    NSString *availability=c[1];
                    availability=[availability stringByReplacingCharactersInRange:NSMakeRange([availability rangeOfString:@"\""].location, availability.length-[availability rangeOfString:@"\""].location) withString:@""];
                    c=[availability componentsSeparatedByString:@"| "];
                    availability=c[1];
                    NSLog(@"Availability: %@",availability);
                    [shipTimes addObject:availability];
                    foundName=NO;
                    foundWatch=NO;
                }
            }
        }
    }
    watchNames=watches;
    watchShipTimes=shipTimes;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_resultsTableView reloadData];
        _loadingSpinner.hidden=YES;
        _loadingLabel.hidden=YES;
        [_loadingSpinner stopAnimation:nil];
    });
}
- (IBAction)changedCountry:(id)sender {
    NSPopUpButton *popUpButton=(NSPopUpButton *)sender;
    NSLog(@"Clicked button: %@",popUpButton.titleOfSelectedItem);
    _loadingSpinner.hidden=NO;
    _loadingLabel.hidden=NO;
    [_loadingSpinner startAnimation:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [self getShipTimesForWatch:_kindButton.indexOfSelectedItem forCountry:_countryButton.titleOfSelectedItem is42mm:NO];
    });
}

@end
