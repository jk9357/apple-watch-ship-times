//
//  ViewController.h
//  Apple Watch Ship Times
//
//  Created by jk9357 on 20/04/2015.
//  Copyright (c) 2015 World Wide Meth Distribution Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController <NSTableViewDataSource,NSTableViewDelegate>

@property (weak) IBOutlet NSTableView *resultsTableView;
@property (weak) IBOutlet NSTextField *loadingLabel;
@property (weak) IBOutlet NSProgressIndicator *loadingSpinner;
@property (weak) IBOutlet NSTableColumn *modelColumn;
@property (weak) IBOutlet NSPopUpButton *countryButton;
@property (weak) IBOutlet NSPopUpButton *kindButton;

@end

