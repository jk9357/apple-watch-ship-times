//
//  main.m
//  Apple Watch Ship Times
//
//  Created by jk9357 on 20/04/2015.
//  Copyright (c) 2015 World Wide Meth Distribution Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
